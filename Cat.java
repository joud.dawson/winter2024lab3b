public class Cat {
    public String eyeColor;
    public double ageInMonths;
    public int boomAmount;

        public Cat(String eyeColor, int ageInMonths, int boomAmount) {
            this.eyeColor = eyeColor;
            this.ageInMonths = ageInMonths;
            this.boomAmount = boomAmount;
        }

        public void sayAge(){
            double ageInYears = ageInMonths/12;
        
            System.out.println("I am " + ageInMonths + " months old or " + ageInYears + " years old. My eyes are " + eyeColor + ".");
    
        }

        public void explode(){
            for (int i = 0; i < 4; i++) {
                System.out.println(3-i);
            }

            for (int i = 0; i < boomAmount; i++) {
                System.out.print("boom");
            }

        }

}
