import java.util.Scanner;

public class VirtualPetApp {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        Cat[] clowder = new Cat[4];

        for (int i = 0; i < clowder.length; i++) {
            System.out.println("Cat " + (i + 1) + " eye color:");
            String eyeColor = input.nextLine();

            System.out.println("The age in months:");
            int ageInMonths = input.nextInt();
            input.nextLine();

            System.out.println("Boom amount:");
            int boomAmount = input.nextInt();
            input.nextLine();

            clowder[i] = new Cat(eyeColor, ageInMonths, boomAmount);
        }

        Cat lastCat = clowder[clowder.length-1];

        // print 3 variables of last cat
        System.out.println(lastCat.eyeColor);
        System.out.println(lastCat.ageInMonths);
        System.out.println(lastCat.boomAmount);

        Cat firstCat = clowder[0];

        firstCat.sayAge();
        firstCat.explode();

    }
}